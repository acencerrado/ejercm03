package ejercArrays;

import java.util.ArrayList;
import java.util.Collections;

public class ejercIntroArrays01 {

	public static void main(String[] args) {
		
		int[]  a = {4,5,6,7};
		int[]  b = new int[4];
		
		for(int i = 0; i < a.length; i++) {
			
			System.out.println(a[i]);
			
		}
		
		for(int i = 0; i < b.length; i++) {
			
			b[i]=8;
			System.out.println(b[i]);
			
		}
		
		int max = 0;
		for(int i = 0; i < a.length; i++) {
			
			if(a[i]>max) {
				max = a[i];
			}
			
		}
		System.out.println(max);
		
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(4);
		list.add(8);
		list.add(7);
		list.add(9);
		list.add(99);
		list.add(2);
		
		System.out.println(list);
		
		Collections.sort(list);
		Collections.reverse(list);
		System.out.println(list.get(0));
		System.out.println(list);
		
		ArrayList<String> listString = new ArrayList<String>();
		
		listString.add("patata");
		listString.add("gatete");
		listString.add("xxxDavidilloxxx");
		listString.add("putoGit");
		listString.add("manzanasGratis");
		listString.add("soyProgramadorFP");
		
		System.out.println(listString);
		
		listString.remove("gatete");
		
		System.out.println(listString);
		
		Collections.sort(listString);
		
		System.out.println(listString);
		
	}

}
