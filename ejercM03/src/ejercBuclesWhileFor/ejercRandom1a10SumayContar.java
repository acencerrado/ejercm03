package ejercBuclesWhileFor;

import java.util.Random;

public class ejercRandom1a10SumayContar {

	public static void main(String[] args) {
		//Hacer un random de 1 a 10, cuando sale 1 el programa se para.
		//Tiene que sumar los números que salen y también tiene que sumar cuantos numeros han salido.
		
		int num = 0; //Inicializamos en un número menor a 1
		int acc = 0; //Acumulador para sumar los valores hasta que sale 1 según while
		int contador = 0; //cuenta los números que han salido del while
		
		while (num!=1)
		{
			Random r = new Random(); //para que respita dentro del while
			num = r.nextInt((10)+1);
			acc = acc + num;
			contador++;
			
			System.out.println(num);
		}
		
		System.out.println(acc);
		System.out.println(contador);
		
	}

}
