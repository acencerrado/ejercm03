package ejercBuclesWhileFor;

public class ejerc1223334444 {

	
	public static void main(String[] args) {
		
		int n = 5;		
		for(int i = 1;i<=n;i++) {		
			for(int j = 1;j<=i;j++) {
				System.out.println(i);
			}
		}
		
		
		
	}
}

//si imprimo j imprimirira: 12345, 12345, 12345, 12345, 12345
//si imprimo i imprimire: 11111,22222,33333,44444,55555

//for (int j=1; j<=i; j++) {  ESTA SERIA LA MANERA DE HACERLO BIEN!!
//en la condició de salida, si per "j" igual o menor a "i" lo que hace es imprimir 1,22,333,4444,5555, 
//pq aquí la "i" lo que hace es en la 1ª ronda vale 1, en la 2a se hace 2 veces, en la 3a se hace 3..etc
// la clave es usar la "i" en j<=n, hacer j<=i
// la i son filas i la j columnas