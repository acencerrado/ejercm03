package ejercBuclesWhileFor;
import java.util.Scanner;

public class ejercWhileBandAlgor1 
{

	public static void main(String[] args) 
	{
			
		//Algorisme que llegeix una seqüència de notes enteres (0..10) fins que llegeix el -1 i ens diu si alguna de les notes era un 10.
				
				
		Scanner sc = new Scanner(System.in);
			
			int nota = sc.nextInt();
			boolean flag = false;
			while (nota != -1) 
			{
				if (nota == 10) 
				{
					flag = true;
				}
				
				nota = sc.nextInt();
			}
			System.out.println(flag);
	}
}

/*// algoritmo que lee hasta que encuentra un -1 y si uno de esos num es 10, nos
// dice cuantos hay

// se introduce un contador;

Scanner sc = new Scanner(System.in);
int nota;
nota = sc.nextInt();

int contador = 0;

boolean flag = false;
// fem codi

while (nota != -1) {
    if (nota == 10) {
        flag = true;
        contador++; // cuando el flag es true entonces sí añado el contador
    }
    // dins de bucle necesito el valor de nota, pq sino sempre ser elvalor inicial d
    // entrada al bucle, si d'inici nota = 9 per exemple, estaria en bucle
    // infinitament
    nota = sc.nextInt();
} // cierro bucle

System.out.println("en tus números tienes " + contador + " veces el num 10");
// sale false si ningun numero es 10
// cuando llego a -1 ha salido algun 10
                                                                           
// sale true cuando al escribir -1 algun numero de esos ha sido el 10

}

}

*/