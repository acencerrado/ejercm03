package ejercBuclesWhileFor;


public class ejercIteratives05 
{

	public static void main(String[] args) 
	{
		//5. Algorisme que calcula la suma dels números parells i els números senars dels números entre 1 i 100.

		int accPar = 0;
		int accImpar = 0;
		
		for (int i = 1; i <= 100; i++)
		{
			if (i % 2 == 0) 
			{
				accPar = i + accPar;
			}
			else
			{
				accImpar = i + accImpar;
			}
		}
		System.out.println("Suma de pares: "+accPar);
		System.out.println("Suma de impares: "+accImpar);
		
	}

}
