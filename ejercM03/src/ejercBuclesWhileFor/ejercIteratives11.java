package ejercBuclesWhileFor;

import java.util.Scanner;

public class ejercIteratives11 
{

	public static void main(String[] args) 
	{
		/*Algorisme que llegeix un número enter N "8 por ejemplo" i ens diu la llista de números que són divisors d’aquest.
		*/
	
		Scanner sc = new Scanner(System.in);
			
		System.out.println("Introduce un número entero");
			
		int num = sc.nextInt();
			
		for (int i = 1; i <= num; i++) 
		{ //iniciamos en 1 y acabamos en 8
			
			if (num%i==0) //módulo de i porque i es lo que se repite
			{
			System.out.println(i); //el syso dentro, fuera imprime el for
			}
				
								
		}

	}

}
