package ejercBuclesWhileFor;


import java.util.Scanner;

public class ejercFerrerosTotales {

	public static void main(String[] args) {
						   
	  Scanner sc = new Scanner(System.in);//lee el número que indiquemos (funciones: syso, Scanner, random)
	       
	     int pisos = sc.nextInt(); 	//Introducimos pisos (nextInt (cajón) = viene de Scanner) (variables: int, double, boolean...)
	     int ferrerosperpis; 		//Será la "i" (variables: if - condicionales (else if, else), switch, for - para, while - mientras)
	     int ac=0; 					//Será acumulador, empieza en 0 para sumar a partir del siguiente
	     for(int i = pisos; i>=1; i--) 
	     { 							//i = pisos, queremos empezar a contar a partir de que i=1, 
	       							//i-- para sumar desde el final a "1 o 0" 
	     ferrerosperpis= i*i; 		// 1*1 (1+0ac) + 2*2 + (4+1ac) + 3*3 (9+5ac) + 4*4 (16+14ac) = (=30) 
	     ac=ac+ferrerosperpis;
	     }
	       
	     System.out.println(ac);
	
	sc.close(); 	
	}
	
}
