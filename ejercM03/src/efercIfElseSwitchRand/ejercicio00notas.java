package efercIfElseSwitchRand;
public class ejercicio00notas {
	public static void main(String[] args) {
	}
}
	/* NOTAS
	
	Clases
	| int números entero | double números con decimales | String caracteres/palabras |	boolean true/false |
	
	Aritméticos
	| + suma | - resta | * multiplicación | / división |
	
	Lógico, relacionales y boleanos
	| > mayor que | < menor que | <> mayor o menor que | != distinto que | == igual que | && and | || or |
	|>= mayor o igual que | <= menor o igual que | .equals igual para String |
	
	Incremento y decremento en cálculos
	
	| ++ incremento; 6++ (7) | -- decremento 6-- (5) | += "nº" 1+=6 (7) | -= "nº" 1-=6 (5) |
	
	Concatenación
	
	| + [ System.out.println("El día"+" "+a+" "+"es el correcto"); (cuando "a" viene de una variable inicializada)
	
	Módulo
	
	(n1%2==0 && n2%2==0) Indica si es número par. Se calcula el módulo de 2 al número, debe dar cero para par.
	
	(n1%2==1 && n2%2==1) Indica si es número impar. Se calcula el módulo de 1 al número, debe dar uno para impar.
	
	NORMAS DE PROGRAMACIÓN DESTINADAS A OBJETOS
	
	1-Declarar antes de usar

	1.1 Tipo/clase
	1.2 Nombre de la variable      “int a = 4” “...= (4 + B * 5)
	1.3 El = a su inicialización
	 *Solo se declara una vez (globalmente)

	2-Sólo hay 4 tipos de líneas/sentencias

    2.1 Algo es = a algo
    2.2 Estructuras de control: if, else, if else, switch, case, break...
    2.3 Funciones, declaraciones de funciones “main”. Void-”Syso”
    2.4 Llaves sueltas }

	3-Dentro de una estructura de control (if (boolean), else (boolean), while (boolean)):
    Aritmetológicas: ==, !=, >, >=, <, <=
    Relacionales: and, or, xor (se usa poco); &&, ||, ^ (se usa poco)

	switch==if, else
	for==while
	
	*/

