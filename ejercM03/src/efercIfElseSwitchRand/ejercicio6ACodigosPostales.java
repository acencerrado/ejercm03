package efercIfElseSwitchRand;

import java.util.Scanner;

public class ejercicio6ACodigosPostales 
{
	
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Indica un Código Postal de Sabadell");
		
		String cp = sc.nextLine();
		String barrio = "";
		
		switch (cp) 
		{
		case "08021":
			barrio = "centre";
			break;
		case "08202":
			barrio = "Torre Romeu";
			break;
		case "08203":
			barrio = "Eixample";
			break;
		case "08204":
			barrio = "Creu de Barberà";
			break;
		case "08205":
			barrio = "Gràcia";
			break;
		case "08206":
			barrio = "Can Rull";
			break;
		case "08207":
			barrio = "Ca n'Oriac";
			break;
		case "08208":
			barrio = "Creu Alta";
			break;
		default:
			System.out.println("No es un barrio de Sabadell");
			break;
		}
		System.out.println(barrio);
		
		sc.close();
	}

}
