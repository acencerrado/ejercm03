package efercIfElseSwitchRand;

public class ejercicio01examen {

		/*
		Els colors es generen a partir de tres colors base: Magenta, Groc i Cian. 
		Implementeu un programa en Java que
		Demana una lletra corresponent l primer color base: M: Magenta o N: Cap color
		Demana una segona lletra coresponent al segon color base:  G: Groc o N: Cap color
		Demana la tercera lletra corresponent al tercer color base: C: Cian o N: Cap color
		Valida que l’entrada de dades sigui correcte. Si no és així no farà res mes i avisarà per pantalla.
		Mostra per pantalla el nou color generat d’acord amb la següent taula:

		Color1-Color2-Color3-Color Resultat
		M-N-N-Magenta
		M-G-NVermell
		M-G-C-Negre
		N-G-N-Groc
		N-G-C-Verd
		N-N-C-Cian
		M-N-C-Violeta
		N-N-N-Blanc
		*/

		private static void exercici3() {
			// TODO Auto-generated method stub
			boolean r = true;
			boolean b = true;
			boolean g = true;
			
			if(r) {  //si r
				if(g) {  //si rg
					if(b) {  //si rgb
						System.out.println("blanc");
					}else {  //rg!b
						System.out.println("groc");
					}
				}else { //no r!g
					if(b) {  //si r!gb
						System.out.println("magenta");
					}else {  //r!g!b
						System.out.println("vermell");
					}
				}
			}else {  // !r
				if(g) {  //si !rg
					if(b) {  //!rgb
						System.out.println("cyan");
					}else {  //!rg!b
						System.out.println("verde");
					}
				}else { // !r!g
					if(b) {  //si !r!gb
						System.out.println("blau");
					}else {  //!r!g!b
						System.out.println("negre");
					}
				}
			}

			
	}
}



