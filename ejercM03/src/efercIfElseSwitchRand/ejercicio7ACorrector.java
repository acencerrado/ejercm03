package efercIfElseSwitchRand;

import java.util.Scanner;

public class ejercicio7ACorrector {

	/*
	Cada alumno tendrá una nota de ejercicio 1, (10%), ejercicio 2 (30%) y del examen (60%).
	Son notas que pueden tener decimales (double). No es necesario introducir por teclado.
	Si cualquiera de los dos ejercicios no está entregado, y por tanto tiene un 0, la UF queda suspendida.
	También queda suspendida si el examen está por debajo de 4.
	En caso contrario se hace la media ponderada. Si es igual o superior a 5 está aprobado.
	Si no está suspendido. Se muestra por pantalla “Aprobado” o “Suspendido”.
	Pista: La mitja ponderada es hace así:  (nota1*0,1+nota2*0,3+notaexamen*0,6)
	*/
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Dime la nota del ejercicio 1: ");
		double e1 = sc.nextDouble();
		
		System.out.println("Dime la nota del ejercicio 2: ");
		double e2 = sc.nextDouble();
		
		System.out.println("Dime la nota del examen: ");
		double examen = sc.nextDouble();
		Double ponderada = 0.0;

		if (e1 == 0 || e2 == 0 || examen <= 4)
		{
			System.out.println("Suspendido!");
		}
		else
		{
			ponderada =  (e1*0.1)+(e2*0.3)+(examen*0.6);
			if (ponderada >= 5.0)
			{
				System.out.println("Aprobado!");
			}
			else
			{
				System.out.println("Suspendido!");
			}
		}
		sc.close();
	}

}
