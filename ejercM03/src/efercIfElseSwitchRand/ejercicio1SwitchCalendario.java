package efercIfElseSwitchRand;

import java.util.Scanner;

public class ejercicio1SwitchCalendario {

	/*
	Implementeu un programa en Java que demani un dia i un mes qualsevol del calendari 
	i ens digui quants dies falten per arribar a fi de mes. 
	El programa validarà qualsevol errada en les dades d’entrada.
	Considereu que febrer sempre és de 28 dies. Exemples:                                                                                                  
		Entrada: dia – 23 mes – 2    
		Sortida: Falten 5 dies               
		Entrada  dia - 31 mes – 2
		Sortida: ERROR en les dades d’entrada
	*/
	
	public static void main(String[] args) {

		System.out.println("Indica mes");
		
		Scanner sc = new Scanner(System.in);
		int mes = sc.nextInt();
		
		System.out.println("Indica día");
		int dia = sc.nextInt();
		
		int diasDelMes = 0; //muy importante, inicia la cuenta de los días meses		
		int resultado = 0; //muy importante, inicia la cuenta de los días meses
				
		switch (mes) {
		case 1:
		case 3:
		case 5:	
		case 7:
		case 8:
		case 10:
		case 12:
			diasDelMes = 31;
			break;
		case 2:
			diasDelMes = 28;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			diasDelMes = 30;
			break;
		default:
		System.out.println("Indica el mes correcto");
			break;
		}
		
		if (dia>diasDelMes) {
			System.out.println("Indica el día correcto");
		}
		else {
			System.out.println("Faltan "+(diasDelMes-dia)+" días para acabar el mes.");
		}
	}

}
