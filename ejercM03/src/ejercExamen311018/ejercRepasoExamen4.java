package ejercExamen311018;

import java.util.Random;
import java.util.Scanner;

public class ejercRepasoExamen4 {

	public static void main(String[] args) {

		//llebretortuga();
		//buclesRandoms();
		//repetirLetra("patata");
		menu();

	}

	private static void llebretortuga() {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		int vl=sc.nextInt();
		int vt=sc.nextInt();
		int meta=sc.nextInt();
		int ventaja=sc.nextInt();
		
		int tortuga = 0;
		int liebre = -ventaja;
		
		int cont = 0;
		
		while(tortuga<=meta && liebre <= meta) {
			tortuga = tortuga + vt;
			liebre = liebre + vl;
			cont++;
		}
		if(liebre>tortuga) {
			System.out.println("gana liebre");
		}else if (tortuga>liebre) {
			System.out.println("gana tortuga");
		}else {
			System.out.println("empate");
		}
		System.out.println("en "+cont+" segundos");
	}

	private static void menu() {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		int valor = 0;
		boolean sortir = false;
		while (!sortir) {
			System.out.println("selecciona opcio");	
			System.out.println("a escollir valor, b comptar, c insultar d sortir");
			String opcio = sc.nextLine();  //a,b,c,d
			switch (opcio) {

			case "a":
				System.out.println("introduce valor");
				valor = sc.nextInt();
				sc.nextLine();
				break;
			case "b":
				for (int i = 0; i < valor; i++) {
					System.out.println(i);
				}
				break;
			case "c":
				System.out.println("xxxdavililloxxx feeder");
				break;
			case "d":
				sortir = true;
				break;
			default:
				System.out.println("caraesparto");

			}
		}
	}

	private static void repetirLetra(String string) {
		
		//comparant la posico actual amb la seg�ent
		for(int i = 0; i<string.length()-1; i++) {
			//i SEMPRE es la posicio actual
			char posicioActual = string.charAt(i);
			char posicioSeg = string.charAt(i+1);
			if(posicioActual == posicioSeg) {
				System.out.println("s'ha repetit");
			}
		}
		
		//comparant la posico actual amb l'anterior
		
		for(int i = 1; i<string.length(); i++) {
			//i SEMPRE es la posicio actual
			char posicioActual = string.charAt(i);
			char posicioAnt = string.charAt(i-1);
			if(posicioActual == posicioAnt) {
				System.out.println("s'ha repetit");
			}
		}
		
		

	}

	private static void buclesRandoms() {
		// generem nums aleatoris del 1 al 10 fins a l'1. Despr�s sumarlos i contarlos

		// Creem un random
		Random r = new Random();
		// forma, inicialitzar i posarho al final
		int num = r.nextInt(10) + 1;
		int cont = 0;
		int acc = 0;
		while (num != 1) {
			cont++;
			acc = acc + num;
			System.out.println(num);
			num = r.nextInt(10) + 1;

		}

		// no inicialitzar i posarho al principi
		num = 0;
		cont = 0;
		acc = 0;
		boolean flag = false;
		while (!flag) {
			num = r.nextInt(10) + 1;
			cont++;
			acc = acc + num;
			System.out.println(num);
			if(num==1) {
				flag = true;
			}
	
		}

	}

}
