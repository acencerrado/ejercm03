package ejercExamen311018;
import java.util.Random;
public class ejerc3DobleBofetonContadoresRandom1a10 
{
	public static void main(String[] args) 
	{
		//3 doble bofetón - Contadores - Random 1 a 10
		//Hacer un random de 1 a 10, cuando sale 1 el programa se para.
		//Tiene que sumar los números que salen y también tiene que sumar cuantos numeros han salido.
				
			int num = 0; //Inicializamos en un número menor a 1
			int acc = 0; //Acumulador para sumar los valores hasta que sale 1 según while
			int contador = 0; //cuenta los números que han salido del while
				
			while (num!=1)
			{
				Random r = new Random(); //para que repita dentro del while
				num = r.nextInt((10)+1); //para que haga radom de 0 a 9 (10) pero al ser +1 arranca en 1
				acc = acc + num; //acumulador, suma todos los n�meros hasta que sale 1
				contador++; //contador, cuenta los n�mes que han salido hasta que sale 1
					
				System.out.println(num); //Imprime todos los n�meros hasta que sale el 1
			}
				
			System.out.println(acc);
			System.out.println(contador);
	}
}