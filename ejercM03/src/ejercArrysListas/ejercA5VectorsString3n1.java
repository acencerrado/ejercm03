package ejercArrysListas;

import java.util.ArrayList;
import java.util.Scanner;

public class ejercA5VectorsString3n1 
{

	public static void main(String[] args) 
	{
		
		//Fer un programa per omplir un vector de 10 elements de valors enters. 
		//Després demanarà a l’usuari un nº enter i el programa informarà de quants 
		//valors són més grans, quants són més petits i quants són iguals al número introduït. Array/Lista
	
		ArrayList<Integer> lints = new ArrayList<Integer>();
		lints.add(1);
		lints.add(2);
		lints.add(3);
		lints.add(4);
		lints.add(5);
		lints.add(6);
		lints.add(7);
		lints.add(8);
		lints.add(9);
		lints.add(10);
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce un número");
		int numindicado = sc.nextInt();
				
		int contadormasgrandes=0;
		int contadormaspeques=0;
		int contadornumiguales=0;
		
		for (int i = 0; i < lints.size(); i++) 
		{
			if(numindicado>lints.get(i)) 
			{
				contadormasgrandes++;
			}
						
			if(numindicado<lints.get(i)) 
			{
				contadormaspeques++;
			}
						
			if(numindicado==lints.get(i)) 
			{
				contadornumiguales++;
			}
									
		}
		
		System.out.println("El vector tiene "+contadormasgrandes+" valores más grandes.");
		System.out.println("El vector tiene "+contadormaspeques+" valores más pequeños.");
		System.out.println("El vector tiene "+contadornumiguales+" valores iguales.");
	
	}

}
