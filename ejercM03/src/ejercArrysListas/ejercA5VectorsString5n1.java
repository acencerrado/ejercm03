package ejercArrysListas;

import java.util.ArrayList;
import java.util.Collections;

public class ejercA5VectorsString5n1 {

	public static void main(String[] args) {
		
		//Fer un programa per omplir un vector de 10 elements de valors enters positius. 
		//El programa ha de garantir que els elements estan en ordre ascendent. 
		//Exemple: 
		//Seqüència vàlida  2,5,7,8,16,22,24,26,27,29. 
		//Sequència incorrecta: 2,3,5,6,7,6,... Lista
		
		ArrayList<Integer> lints1 = new ArrayList<Integer>();
		lints1.add(1);
		lints1.add(2);
		lints1.add(3);
		lints1.add(5);
		lints1.add(4);
		lints1.add(6);
		lints1.add(7);
		lints1.add(8);
		lints1.add(9);
		lints1.add(10);
		
		ArrayList<Integer> lints2 = new ArrayList<Integer>();
		lints2.addAll(lints1);
		Collections.sort(lints2);
		
		System.out.println(lints1);
		System.out.println(lints2);
		System.out.println("Di si los elementos son iguales: "+lints2.equals(lints1));
	}

}
