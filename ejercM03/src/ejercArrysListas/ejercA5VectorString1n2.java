package ejercArrysListas;

import java.util.ArrayList;
import java.util.Random;

public class ejercA5VectorString1n2 
{

	public static void main(String[] args) 
	{
		// Fer un programa que omplirà un vector de 6 elements de números aleatoris
		// entre 1 i 49 i després mostrarà el contingut del vector per pantalla.

		ArrayList<Integer> lstr = new ArrayList<Integer>();
		int longitud = 6;

		for (int i = 0; i < longitud; i++) 
		{
			Random r = new Random();
			lstr.add(r.nextInt(49) + 1);
		}
		System.out.println(lstr);
	}

}
