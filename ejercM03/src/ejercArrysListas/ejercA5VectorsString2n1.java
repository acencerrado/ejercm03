package ejercArrysListas;

import java.util.ArrayList;
import java.util.Scanner;

public class ejercA5VectorsString2n1 {

	public static void main(String[] args) {
		
		//Fer un programa per omplir un vector de 10 elements de valors enters. 
		//Després demanarà a l’usuari un nº enter i el programa respondrà si aquest 
		//valor es troba al vector o no. Listas

		ArrayList<Integer> lints = new ArrayList<Integer>();
		lints.add(1);
		lints.add(2);
		lints.add(3);
		lints.add(4);
		lints.add(5);
		lints.add(6);
		lints.add(7);
		lints.add(8);
		lints.add(9);
		lints.add(10);
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce un número");
		int numindicado = sc.nextInt();
		
		System.out.println(lints.contains(numindicado));
		
	}

}
