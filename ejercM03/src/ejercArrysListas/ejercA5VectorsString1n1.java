package ejercArrysListas;

public class ejercA5VectorsString1n1 
{

	public static void main(String[] args) 
	{
	
		//Fer un programa per omplir un vector de 10 elements de valors enters 
		//i mostrar la suma dels valors que es troben en posicions del vector 
		//múltiples de 3. Array
		
		int[] aints = {1,2,3,4,5,6,7,8,9,10};
		
		int acc = 0;
				
		for (int i = 0; i < aints.length; i++) 
		{
			if (aints[i]%3==0) 
		 
			acc = acc + aints[i];
		}

		System.out.println(acc);
	}

}


